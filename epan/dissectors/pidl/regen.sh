#!/bin/bash
#

set -e
set -u
set -x

rm -f epan/dissectors/pidl/*-stamp
pushd epan/dissectors/pidl/ && make ;popd
exit 0

for file in *.idl */*.idl; do
	echo "Generating dissector for $file"
	../../../tools/pidl/pidl --includedir . --ws-parser -- $file;
done
mv packet-dcerpc*.* ../
