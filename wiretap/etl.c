/* etl.c
 *
 * Basic support for Microsoft ETL traces
 * Copyright (c) 2019 by Aurelien Aptel <aurelien.aptel@gmail.com>
 *
 * Wiretap Library
 * Copyright (c) 1998 by Gilbert Ramirez <gram@alumni.rice.edu>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "config.h"

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "wtap-int.h"
#include "file_wrappers.h"

/*
 * ETL files are Windows native traces. They can be generated using
 * netsh:
 *
 *     netsh trace start tracefile=c:\mytrace.etl capture=yes
 *     netsh trace stop
 *
 * They are quite versatile: they store all sorts of system
 * information (TCP/IP stack state, processes running, ...) and system
 * events (syscalls, kernel stacks, ...), including network
 * traffic. It's pretty much the equivalent of strace, ftrace, /proc/
 * and tcpdump all bundled into one file.
 *
 * The API to consume and produce those events on a Window system is
 * called ETW and it uses a myriad of different structs, some
 * undocumented.
 *
 * https://docs.microsoft.com/en-us/windows/win32/api/evntcons/
 *
 * ETL files are made of those structs which are simply dumped from
 * memory. The file format remains undocumented but can be figured out
 * by looking at the API struct definitions, hexdumps and some guess
 * work. Microsoft also has its own tool to explore those traces
 * (MessageAnalyzer) which was very useful to double-check some of the
 * findings.
 *
 * Each event producer is called a Provider and they all have a
 * GUID. It seems the Provider responsible for generating the network
 * traffic events is "Microsoft-Windows-NDIS-PacketCapture".
 *
 * Here is a pseudo-grammar of an ETL file
 *
 * ETL        := ETW_BUFFER+
 * ETW_BUFFER := WMI_BUFFER_HEADER EVENT+
 * EVENT      := SYSTEM_TRACE_HEADER TRACE_LOGFILE_HEADER
 *            |  PERFINFO_TRACE_HEADER
 *            |  EVENT_HEADER <-- packets are here
 *            |  EVENT_INSTANCE_HEADER
 *            |  (other ignored types)
 *
 * NETMON files use EVENT_HEADER stuctures similar enough that we can
 * reuse its dissector. But the structure varies enough that we have
 * to introduce a new WTAP_ENCAP_ETL value.
 *
 * This ETL reader reuses existing Wireshark support:
 * - the netmon dissector will dissect the EVENT_HEADER itself.
 * - the messageanalyzer dissector knows about the NDIS Provider and
 *   will handle the ETW NDIS sub-payload.
 */

/*
 * The packet in the trace are out of order unfortunately. This define
 * controls whether we should expose them to wiretap sorted, so that
 * frame number increases correspond to timestamp increases. MS tool
 * MessageAnalyzer does the same thing.
 */
#define SORTED_PACKETS

static int etl_file_type_subtype = -1;

void register_etl(void);

#define WMI_BUFFER_HEADER_SIZE 0x48
#define EVENT_HEADER_SIZE 0x50
#define NDIS_HEADER_SIZE  0x0C
#define GUID_SIZE 0x10
#define WMI_BUFFER_MAX_SIZE 0x800000 /* 8MB */

/* #define DEBUG_ETL */

#ifdef DEBUG_ETL
/* workaround for stupid checkAPIs checks *sigh*... */
#define DBG(...) g_ ## debug(__VA_ARGS__)
#define WARN(...) g_ ## warning(__VA_ARGS__)
#else
#define DBG(...) do {} while (0)
#define WARN(...) do {} while (0)
#endif

struct wmi {
	uint32_t size;
	uint32_t useful_size;
	uint16_t buffer_flag;
	int64_t time_start;
};

struct etl_index {
	int64_t etime;
#ifdef SORTED_PACKETS
	int64_t sorted_off;
#endif
	int64_t off;
	uint32_t size;
};

struct etl {
	int64_t start_time;
	GArray *pkts;
	bool is_indexed;
};

wtap_open_return_val etl_open(wtap *wth, int *err, char **err_info);
static int read_wmi(struct wmi *wmi, wtap *wth, bool next, int *err);
static int etl_index_all_packets(struct etl *etl, wtap *wth, int *err);
static void etl_free(struct etl *etl);
static struct etl_index *etl_find_index(struct etl *etl, int64_t off);
static bool etl_seek_read(wtap *wth, int64_t seek_off, wtap_rec *rec, int *err, char **err_info);
static bool etl_read_rec(struct etl *etl, struct etl_index *e, FILE_T fh, wtap_rec *rec, int *err, char **err_info);
static bool etl_read(wtap *wth, wtap_rec *rec, int *err, char **err_info, int64_t *data_offset);

enum {
      ETL_OK,  /* seems ok */
      ETL_EOF, /* reached EOF */
      ETL_BAD, /* bad input */
      ETL_ERR, /* io failure */
};

static int
read_wmi(struct wmi *wmi, wtap *wth, bool next, int *err)
{
	int64_t rc;
	int64_t start, off;
	uint8_t buf[WMI_BUFFER_HEADER_SIZE];

	/*
	  4 uint32 BufferSize
	  4 uint32 SavedOffset
	  4 uint32 CurrentOffset
	  4  int32 ReferenceCount
	  8  int64 TimeStamp
	  8  int64 SequenceNumber
	  8 uint64 ClockType/Frequency
	  1  uint8 ProcessorNumber
	  1  uint8 Alignment
	  2 uint16 LoggerId
	  4 uint32 BufferState
	  4 uint32 Offset <--- useful size
	  2 uint16 BufferFlag
	  2 uint16 BufferType
	  8  int64 StartTime
	  8  int64 StartPerfClock
	 */

	start = file_tell(wth->fh);
	rc = file_read(buf, sizeof(buf), wth->fh);
	if (rc != sizeof(buf)) {
		if (rc == 0 && file_eof(wth->fh))
			return ETL_EOF;
		return ETL_BAD;
	}

	wmi->size = pletoh32(buf + 0);
	wmi->time_start = (int64_t)pletoh64(buf + 4+4+4+4);
	wmi->useful_size = pletoh32(buf + 4+4+4+4+8+8+8+1+1+2+4);
	wmi->buffer_flag = pletoh16(buf + 4+4+4+4+8+8+8+1+1+2+4+4);

	/* check min/max sizes */
	if (wmi->size < WMI_BUFFER_HEADER_SIZE || wmi->size > WMI_BUFFER_MAX_SIZE)
		return ETL_BAD;

	/* alignement and useful size */
	if (wmi->size % 8 != 0 || wmi->useful_size > wmi->size)
		return ETL_BAD;

	/* overflow */
	if (start > INT64_MAX - (int64_t)wmi->size)
		return ETL_BAD;

	off = start + (int64_t)wmi->size;
	if (next) {
		rc = file_seek(wth->fh, off, SEEK_SET, err);
		if (rc < 0 || err)
			return ETL_ERR;
	}
	return ETL_OK;
}

enum event_header_type {
    TRACE_HEADER_TYPE_SYSTEM32       = 1,
    TRACE_HEADER_TYPE_SYSTEM64       = 2,
    TRACE_HEADER_TYPE_COMPACT32      = 3,
    TRACE_HEADER_TYPE_COMPACT64      = 4,
    TRACE_HEADER_TYPE_FULL_HEADER32  = 10,
    TRACE_HEADER_TYPE_INSTANCE32     = 11,
    TRACE_HEADER_TYPE_TIMED          = 12,  /* not used */
    TRACE_HEADER_TYPE_ERROR          = 13,  /* error while logging event */
    TRACE_HEADER_TYPE_WNODE_HEADER   = 14,  /* not used */
    TRACE_HEADER_TYPE_MESSAGE        = 15,
    TRACE_HEADER_TYPE_PERFINFO32     = 16,
    TRACE_HEADER_TYPE_PERFINFO64     = 17,
    TRACE_HEADER_TYPE_EVENT_HEADER32 = 18,
    TRACE_HEADER_TYPE_EVENT_HEADER64 = 19,
    TRACE_HEADER_TYPE_FULL_HEADER64  = 20,
    TRACE_HEADER_TYPE_INSTANCE64     = 21
};

static void
etl_add_index(struct etl *etl, int64_t off, int64_t etime, uint32_t size)
{
	struct etl_index v = {
	      .off = off,
	      .etime = etime,
	      .size = size,
	};

	g_array_append_val(etl->pkts, v);
}

#ifdef SORTED_PACKETS
static int
pkt_cmp(const void *a, const void *b)
{
	int64_t ta = ((const struct etl_index *)a)->etime, tb = ((const struct etl_index *)b)->etime;
	return ta > tb ? 1 : (ta < tb ? -1 : 0);
}

static void
etl_sort(struct etl *etl)
{
	unsigned i;
	int64_t sorted_off = 0;

	g_array_sort(etl->pkts, pkt_cmp);

	for (i = 0; i < etl->pkts->len; i++) {
		struct etl_index *e = &g_array_index(etl->pkts, struct etl_index, i);
		e->sorted_off = sorted_off;
		sorted_off += e->size;
	}
}
#endif

static int
etl_index_all_packets(struct etl *etl, wtap *wth, int *err)
{
	int64_t rc;
	struct wmi wmi;
	uint8_t ev_type;
	int64_t wmi_start, ev_start, ev_size, off, pad, etime;
	uint8_t buf[EVENT_HEADER_SIZE];

	wmi_start = off = 0;

	while (!(rc = read_wmi(&wmi, wth, false, err))) {
		DBG("ETL: WMI 0x%" PRIx64, wmi_start);
		DBG("ETL: WMI size 0x%x", wmi.size);

		if (wmi.buffer_flag & 0x40) {
			WARN("ETL: WMI is compressed");
			goto next_wmi;
		}

		while (off < wmi_start + wmi.useful_size) {
			bool ok = false;
			ev_start = off = file_tell(wth->fh);
			DBG("ETL: EV 0x%" PRIx64, ev_start);

			rc = file_read(buf, 6, wth->fh);
			if (rc != 6)
				return ETL_BAD;

			ev_type = buf[2];

			switch (ev_type) {
			case TRACE_HEADER_TYPE_SYSTEM32:
			case TRACE_HEADER_TYPE_SYSTEM64:
			case TRACE_HEADER_TYPE_COMPACT32:
			case TRACE_HEADER_TYPE_COMPACT64:
			case TRACE_HEADER_TYPE_PERFINFO32:
			case TRACE_HEADER_TYPE_PERFINFO64:
				ev_size = pletoh16(buf + 4);
				goto next_event;
			case TRACE_HEADER_TYPE_EVENT_HEADER32:
			case TRACE_HEADER_TYPE_EVENT_HEADER64:
				ev_size = pletoh16(buf + 0);
				break;
			default:
				ev_size = pletoh16(buf + 0);
				goto next_event;
			}

			/* EVENT_HEADER
			   2 uint16 Size
			   1  uint8 Type
			   1  uint8 MarkerFlags
			   2 uint16 Flags
			   2 uint16 EventProperty
			   4 uint32 ThreadId
			   4 uint32 ProcessId
			   8  int64 TimeStamp
			   16  GUID ProviderId
			   struct EVENT_DESCRIPTOR {
			   2            uint16 Id
			   1             uint8 Version
			   1             uint8 Channel
			   1             uint8 Level
			   1             uint8 Opcode
			   2            uint16 Task
			   8            uint64 Keyword
			   } EventDescriptor
			   4 uint32  KernelTime
			   4 uint32  UserTime
			   16  GUID  ActivityId
			*/

			rc = file_seek(wth->fh, ev_start, SEEK_SET, err);
			if (rc != ev_start)
				return ETL_ERR;

			rc = file_read(buf, EVENT_HEADER_SIZE, wth->fh);
			if (rc != EVENT_HEADER_SIZE)
				return ETL_BAD;

			etime = (int64_t)pletoh64(buf+2+1+1+2+2+4+4);
			etime += wmi.time_start;

			ok = true;

		next_event:
			DBG("ETL: EV size 0x%" PRIx64, ev_size);
			if (ev_start > INT64_MAX - ev_size)
				return ETL_BAD;
			if (ev_size <= 6 || ev_size >= UINT32_MAX)
				return ETL_BAD;
			if (ev_start+ev_size > wmi_start + wmi.useful_size)
				return ETL_BAD;

			if (ok)
				etl_add_index(etl, ev_start, etime, (uint32_t)ev_size);

			off = ev_start + ev_size;
			pad = 8 - (off % 8);
			if (pad < 8)
				off += pad;

			rc = file_seek(wth->fh, off, SEEK_SET, err);
			if (rc != off)
				return ETL_ERR;
		}

	next_wmi:
		off = wmi_start + wmi.size;
		rc = file_seek(wth->fh, off, SEEK_SET, err);
		if (rc != off)
			return ETL_ERR;
		wmi_start = off;
	}

	if (rc == 0 || rc == ETL_EOF) {
#ifdef SORTED_PACKETS
		etl_sort(etl);
#endif
		etl->is_indexed = true;
		rc = 0;
	}

	return (int)rc;
}

static void
etl_close(wtap *wth)
{
	struct etl* etl = (struct etl *)wth->priv;
	etl_free(etl);
	wth->priv = NULL;
}

static void
etl_free(struct etl *etl)
{
	if (!etl)
		return;
	g_array_free(etl->pkts, true);
	g_free(etl);
}

wtap_open_return_val
etl_open(wtap *wth, int *err, char **err_info _U_)
{
	/*
	 * ETL files have no magic header. To check for ETL we try to
	 * read a couple of WMI headers and check if some the fields
	 * make sense.
	 */

	struct etl *etl;
	int rc;

	etl = g_new0(struct etl, 1);
	etl->pkts = g_array_sized_new(false, true, sizeof(struct etl_index), 256);

	rc = etl_index_all_packets(etl, wth, err);

	switch (rc) {
	case ETL_OK:
		break;
	case ETL_EOF:
		if (!etl->pkts || etl->pkts->len <= 0) {
			etl_free(etl);
			return WTAP_OPEN_NOT_MINE;
		}
		break;
	case ETL_BAD:
		etl_free(etl);
		return WTAP_OPEN_NOT_MINE;
	default:
		etl_free(etl);
		return WTAP_OPEN_ERROR;
	}

	if (file_seek(wth->fh, 0, SEEK_SET, err) < 0) {
		etl_free(etl);
		return WTAP_OPEN_ERROR;
	}

	wth->file_type_subtype = etl_file_type_subtype;
	wth->priv = (void *)etl;
	wth->subtype_read = etl_read;
	wth->subtype_seek_read = etl_seek_read;
	wth->subtype_close = etl_close;
	wth->snapshot_length = 0; /* not known */

	wth->file_encap = WTAP_ENCAP_ETL;
	wth->file_tsprec = WTAP_TSPREC_USEC;

	return WTAP_OPEN_MINE;
}

static struct etl_index *
etl_find_index(struct etl *etl, int64_t off)
{
	int64_t i;

	if (off == 0)
		return &g_array_index(etl->pkts, struct etl_index, 0);

	for (i = 0; i < etl->pkts->len; i++) {
		struct etl_index *e = &g_array_index(etl->pkts, struct etl_index, i);
#ifdef SORTED_PACKETS
		if (e->sorted_off == off)
			return e;
#else
		if (e->off >= off)
			return e;
#endif
	}

	return NULL;
}

static bool etl_seek_read_handle(struct etl *etl, FILE_T fh, int64_t off,
			    int64_t *data_offset, wtap_rec *rec,
			    int *err, char **err_info)
{
	struct etl_index *e = etl_find_index(etl, off);
	bool r;

	if (!e)
		return false;

#ifdef SORTED_PACKETS
	*data_offset = e->sorted_off;

	if (file_seek(fh, e->off, SEEK_SET, err) == -1)
		return false;

	r = etl_read_rec(etl, e, fh, rec, err, err_info);
	if (!r)
		return false;

	if (file_seek(fh, e->sorted_off + e->size, SEEK_SET, err) == -1)
		return false;
#else
	*data_offset = e->off;

	if (file_seek(fh, e->off, SEEK_SET, err) == -1)
		return false;

	r = etl_read_rec(etl, e, fh, rec, err, err_info);
#endif
	return r;
}

/* Read the next packet */
static bool
etl_read(wtap *wth, wtap_rec *rec, int *err,
	 char **err_info, int64_t *data_offset)
{
	struct etl *etl = (struct etl *)wth->priv;
	int64_t off = file_tell(wth->fh);

	return etl_seek_read_handle(etl, wth->fh, off, data_offset, rec, err, err_info);
}

static bool
etl_seek_read(wtap *wth, int64_t seek_off, wtap_rec *rec,
	      int *err, char **err_info)
{
	struct etl *etl = (struct etl *)wth->priv;
	int64_t data_offset;

	return etl_seek_read_handle(etl, wth->random_fh, seek_off, &data_offset, rec, err, err_info);
}

static bool
etl_read_rec(struct etl *etl _U_, struct etl_index *e, FILE_T fh, wtap_rec *rec,
	     int *err, char **err_info)
{
	uint32_t size = e->size;

	if (e->size < NDIS_HEADER_SIZE || e->size > WTAP_MAX_PACKET_SIZE_STANDARD) {
		*err = WTAP_ERR_BAD_FILE;
		return false;
	}

	rec->rec_type = REC_TYPE_PACKET;
	rec->presence_flags = WTAP_HAS_TS;
	filetime_to_nstime(&rec->ts, e->etime);
	rec->rec_header.packet_header.len = size;
	rec->rec_header.packet_header.caplen = size;
	rec->rec_header.packet_header.pkt_encap = WTAP_ENCAP_ETL;

	return wtap_read_bytes_buffer(fh, &rec->data, size, err, err_info);
}

static const struct supported_block_type etl_blocks_supported[] = {
	/*
	 * We support packet blocks, with no comments or other options.
	 */
	{ WTAP_BLOCK_PACKET, MULTIPLE_BLOCKS_SUPPORTED, NO_OPTIONS_SUPPORTED }
};

static const struct file_type_subtype_info etl_info = {
	"Windows ETL", "etl", "etl", NULL,
	true, BLOCKS_SUPPORTED(etl_blocks_supported),
	NULL, NULL, NULL
};

void register_etl(void)
{
	etl_file_type_subtype = wtap_register_file_type_subtype(&etl_info);
}

/*
 * Editor modelines  -  https://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 8
 * tab-width: 8
 * indent-tabs-mode: t
 * End:
 *
 * vi: set shiftwidth=8 tabstop=8 noexpandtab:
 * :indentSize=8:tabSize=8:noTabs=false:
 */
