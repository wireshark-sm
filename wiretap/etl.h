/* etl.h
 *
 * Basic support for Microsoft ETL traces
 * Copyright (c) 2019 by Aurelien Aptel <aurelien.aptel@gmail.com>
 *
 * Wiretap Library
 * Copyright (c) 1998 by Gilbert Ramirez <gram@alumni.rice.edu>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 */

#ifndef __ETL_H__
#define __ETL_H__

#include <glib.h>
#include "wtap.h"
#include "ws_symbol_export.h"

wtap_open_return_val etl_open(wtap *wth, int *err, gchar **err_info);

#endif
